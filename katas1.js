function oneThroughTwenty() {
    const numbers = [];
    for (let i = 1; i < 21; i++) numbers.push(i);
    
    return numbers;
}

function evensToTwenty() {
    const numbers = []
    for(let i = 2; i < 21; i+=2) numbers.push(i)

    return numbers;
}

function oddsToTwenty() {
    const numbers = []
    for (let i = 1; i < 21; i+=2) numbers.push(i);


    return numbers;
}

function multiplesOfFive() {
    const numbers = []
    for (let i = 1; i < 21; i++) numbers.push(i * 5)
    return numbers;
}

function squareNumbers() {
    const numbers = []
    for (let i = 1; i < 11; i++) numbers.push(i * i);

    return numbers;
}

function countingBackwards() {
    const numbers = []
    for (let i = 20; i > 0; i--) numbers.push(i);

    return numbers;
}

function evenNumbersBackwards() {
    const numbers = []
    for (let i = 20; i > 0; i-=2) numbers.push(i);

    return numbers;
}

function oddNumbersBackwards() {
    const numbers = []
    for (let i = 19; i > 0; i-=2) numbers.push(i);

    return numbers;
}

function multiplesOfFiveBackwards() {
    const numbers = []
    for (let i = 20; i > 0; i--) numbers.push(i * 5);

    return numbers;
}

function squareNumbersBackwards() {
    const numbers = []
    for (let i = 10; i > 0; i-- ) numbers.push(i*i);

    return numbers;
}